<?php
/**
 * MyResearch Controller
 *
 * PHP version 5
 *
 * Copyright (C) Villanova University 2010.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category VuFind
 * @package  Controller
 * @author   Demian Katz <demian.katz@villanova.edu>
 * @author   Robert Lange <lange@ub.uni-leipzig.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://vufind.org Main Site
 */
namespace finc\Fid\Licenses\Controller;

/**
 * Controller for the user account area.
 *
 * @category VuFind
 * @package  Controller
 * @author   Demian Katz <demian.katz@villanova.edu>
 * @author   Robert Lange <lange@ub.uni-leipzig.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://vufind.org Main Site
 */
class MyResearchController extends \VuFind\Controller\MyResearchController
{

    /**
     * Show links for Access to Press Databases, E-Books
     *
     * @return mixed
     * @throws \VuFind\Exception\ILS
     */
    public function licensesAction()
    {
        if (!($user = $this->getUser())) {
            return $this->forceLogin();
        }

        $patron = $this->catalogLogin();
        if (!$this->hasPermissionForLicenses($patron)) {
            return $this->forwardTo('MyResearch', 'Profile');
        }

        $view = $this->createViewModel(['user' => $user]);
        $catalog = $this->getILS();
        $this->addAccountBlocksToFlashMessenger($catalog, $patron);
        $view->licenses = $this->getLicenses($patron);

        $view->setTemplate('myresearch/licenses');
        return $view;
    }

    /**
     * Get links for licensed resources
     *
     * @param array $patron Patron details
     *
     * @return mixed
     */
    public function getLicenses($patron)
    {
        $licenses = false;

        if ($this->hasPermissionForLicenses($patron)) {
            $config = $this->getConfig();
            $configLicenses = $config['MediaLicenses'] ?? [];

            if (count($configLicenses) > 0) {
                $licenses = [];
                $i = 0;
                foreach ($configLicenses as $key => $value) {
                    $licenses[$i]['name'] = $key;
                    $licenses[$i]['url'] = $value;
                    $licenses[$i]['desc'] = $this->translate('finc-fid-licenses::' . $key . '_desc', [], '');
                    $i++;
                }
            }
        }

        return $licenses;
    }

    /**
     * Check if the user has permission to access licensed resources
     *
     * @param array $patron Patron details
     *
     * @return bool
     */
    public static function hasPermissionForLicenses($patron)
    {
        if (!is_array($patron)) {
            return false;
        }

        if (!isset($patron['accessLevel'])) {
            return false;
        }

        return $patron['accessLevel'] == "default";
    }
}
