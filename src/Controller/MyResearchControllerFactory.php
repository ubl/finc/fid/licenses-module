<?php
/**
 * Service MyResearchControllerFactory
 *
 * PHP version 5.3
 *
 * Copyright (C) Leipzig University Library 2016.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category VuFind
 * @package  Controller
 * @author   Robert Lange <lange@ub.uni-leipzig.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://vufind.org/wiki/development Wiki
 */

namespace finc\Fid\Licenses\Controller;

use Psr\Container\ContainerInterface;
use Zend\ServiceManager\ServiceManager;

class MyResearchControllerFactory
{
    public function __construct()
    {
        return null;
    }

    public function __invoke(ContainerInterface $container)
    {
        /** @var ServiceManager $serviceManager */
        $serviceManager = $container->get(ServiceManager::class);
        return new MyResearchController($serviceManager);
    }
}
