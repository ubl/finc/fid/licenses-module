<?php
/**
 * Permissions view helper
 *
 * PHP version 5
 *
 * Copyright (C) Villanova University 2010.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category VuFind
 * @package  View_Helpers
 * @author   Demian Katz <demian.katz@villanova.edu>
 * @author   Robert Lange <lange@ub.uni-leipzig.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://vufind.org/wiki/development Wiki
 */
namespace finc\Fid\Licenses\View\Helper;

use finc\Fid\Licenses\Controller\MyResearchController;

/**
 * Permissions view helper
 *
 * @category VuFind
 * @package  View_Helpers
 * @author   Robert Lange <lange@ub.uni-leipzig.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://vufind.org/wiki/development Wiki
 */
class LicensePermission extends \Zend\View\Helper\AbstractHelper
{
    /**
     * Check if the user has permission to access licensed resources
     *
     * @param array $patron Patron details
     *
     * @return bool
     */
    public function showLicenseLinkInMenu($patron)
    {
        return MyResearchController::hasPermissionForLicenses($patron);
    }
}
