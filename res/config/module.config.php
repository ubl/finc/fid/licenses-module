<?php
/**
 * Copyright (C) 2019 Leipzig University Library
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * @author  Sebastian Kehr <kehr@ub.uni-leipzig.de>
 * @license http://opensource.org/licenses/gpl-2.0.php GNU GPLv2
 */

use finc\Fid\Core\Api\Client;
use finc\Fid\Core\Api\CredentialsStoreFactory;
use finc\Fid\Core\Session\Session;
use finc\Fid\Core\Session\SessionFactory;
use finc\Fid\Core\VuFind\Auth\Authenticator;
use finc\Fid\Core\VuFind\Auth\ILSAuthenticator;
use finc\Fid\Core\VuFind\Auth\ILSAuthenticatorFactory;
use finc\Fid\Core\VuFind\Db\Row\User;
use finc\Fid\Core\VuFind\ILS\Driver;
use finc\Fid\Core\Api\ClientFactory;
use finc\Fid\Core\Api\CredentialsStoreInterface;
use finc\Fid\Licenses\Controller\MyResearchController;
use finc\Fid\Licenses\Controller\MyResearchControllerFactory;
use finc\Fid\Core\Listener\ErrorListener;
use finc\Fid\Core\Listener\ErrorListenerFactory;
use finc\Fid\Core\VuFind\Auth\AuthenticatorFactory;
use finc\Fid\Core\VuFind\Db\Row\UserDelegatorFactory;
use finc\Fid\Core\VuFind\ILS\DriverFactory;
use VuFind\Auth\ILSAuthenticator as BaseILSAuthenticator;
use VuFind\Db\Row\User as BaseUser;
use VuFind\Db\Row\UserFactory;
use Zend\Router\Http\Regex;

$config = [
    'listeners'       => [
        ErrorListener::class
    ],
    'service_manager' => [
        'aliases'    => [
            BaseILSAuthenticator::class => ILSAuthenticator::class
        ],
        'delegators' => [
            ILSAuthenticator::class => [
                ILSAuthenticatorFactory::class,
            ],
        ],
        'factories'  => [
            Client::class                    => ClientFactory::class,
            CredentialsStoreInterface::class => CredentialsStoreFactory::class,
            ErrorListener::class             => ErrorListenerFactory::  class,
            ILSAuthenticator::class          => ILSAuthenticatorFactory::class,
            Session::class                   => SessionFactory::class,
        ],
    ],
    'controllers' => [
        'factories' => [
            MyResearchController::class      => MyResearchControllerFactory::class,
        ],
        'aliases' => [
            'my-research' => MyResearchController::class,
        ],
    ],
    'vufind'          => [
        'plugin_managers' => [
            'auth'       => [
                'factories' => [
                    Authenticator::class => AuthenticatorFactory::class,
                ],
            ],
            'db_row'     => [
                'aliases'    => [
                    BaseUser::class => User::class,
                ],
                'delegators' => [
                    User::class => [
                        UserDelegatorFactory::class,
                    ],
                ],
                'factories'  => [
                    User::class => UserFactory::class,
                ],
            ],
            'ils_driver' => [
                'factories' => [
                    Driver::class => DriverFactory::class,
                ],
            ],
        ],
    ],
    'router'      => [
        'routes' => [
            'myresearch' => [
                'type'          => Regex::class,
                'options'       => [
                    'regex' => '/(?i)myresearch',
                    'spec'  =>'/myresearch'
                ],
                'may_terminate' => true,
                'child_routes'  => [
                    'licenses' => [
                        'type'          => Regex::class,
                        'options'       => [
                            'regex'    => '/(?i)licenses',
                            'defaults' => [
                                'controller' => MyResearchController::class,
                                'action'     => 'licenses',
                            ],
                            'spec'=>'/licenses'
                        ],
                    ],
                ],
            ],
        ],
    ],
];

$routeGenerator = new \VuFind\Route\RouteGenerator();

return $config;
